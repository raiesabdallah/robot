class WaterLevelData {
  final DateTime timestamp;
  final double waterLevel;

  WaterLevelData(this.timestamp, this.waterLevel);
}
