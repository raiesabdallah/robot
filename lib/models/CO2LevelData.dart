class CO2LevelData {
  final DateTime timestamp;
  final double co2Level;

  CO2LevelData(this.timestamp, this.co2Level);
}
