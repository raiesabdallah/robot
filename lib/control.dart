import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

import 'Dashboard .dart';
import 'manual.dart';

class Control extends StatelessWidget {
  final DatabaseReference _database = FirebaseDatabase.instance.reference();

  void _updateAutomaticState(bool state) {
    _database.child('automaticState').set(state);
  }

  void _updateManualState(bool state) {
    _database.child('manualState').set(state);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.blue, Colors.purple],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: const Text('Robot de Détection et Extinction des Flammes'),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElevatedButton(
                onPressed: () {
                  _updateAutomaticState(true); // Update automaticState to true
                  _updateManualState(false); // Update manualState to false

                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Dashboard()),
                  );
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.redAccent,
                  onPrimary: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
                child: const Text('Automatic', style: TextStyle(fontSize: 24)),
              ),
              const SizedBox(height: 50),
              ElevatedButton(
                onPressed: () {
                  _updateAutomaticState(
                      false); // Update automaticState to false
                  _updateManualState(true); // Update manualState to true

                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ManualPage()),
                  );
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.lightGreen,
                  onPrimary: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
                child: const Text('Manual', style: TextStyle(fontSize: 24)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
