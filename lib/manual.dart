import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class ManualPage extends StatefulWidget {
  @override
  _ManualPageState createState() => _ManualPageState();
}

class _ManualPageState extends State<ManualPage> {
  bool _isPumpOn = false;
  String _robotDirection = '';
  final DatabaseReference _database = FirebaseDatabase.instance.reference();

  void updateDatabase(String key, dynamic value) {
    _database.child(key).set(value);
  }

  void _setRobotDirection(String direction) {
    setState(() {
      _robotDirection = direction;
      updateDatabase('robotDirection', direction);
    });
  }

  void _togglePump() {
    setState(() {
      _isPumpOn = !_isPumpOn;
      updateDatabase('pumpStatus', _isPumpOn);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manual Control'),
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.blue, Colors.purple],
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Video stream container
              Container(
                width: 300, // Adjust the width as needed
                height: 200, // Adjust the height as needed
                color: Colors.black, // Placeholder color
                // Add your video stream widget here
              ),
              const SizedBox(
                  height: 20), // Add spacing between video stream and buttons
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () => _setRobotDirection('forward'),
                        child: const Icon(Icons.arrow_upward),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () => _setRobotDirection('left'),
                        child: const Icon(Icons.arrow_left),
                      ),
                      ElevatedButton(
                        onPressed: () => _setRobotDirection('right'),
                        child: const Icon(Icons.arrow_right),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () => _setRobotDirection('backward'),
                        child: const Icon(Icons.arrow_downward),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 30),
              Text(
                'Robot direction: $_robotDirection',
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: _togglePump,
                style: ElevatedButton.styleFrom(
                  primary: _isPumpOn ? Colors.green : Colors.red,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Icon(Icons.power_settings_new),
                    SizedBox(width: 10),
                    Text('Pump'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
