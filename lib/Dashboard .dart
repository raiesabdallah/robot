import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:new_robot/menu.dart';
import 'package:new_robot/models/CO2LevelData.dart';

import 'control.dart';
import 'models/WaterLevelData.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  double? _waterLevel;
  double? _CO2Level;
  final DatabaseReference _realtimeDatabase =
  FirebaseDatabase.instance.reference();
  final List<WaterLevelData> _waterLevelDataList = [];
  final List<CO2LevelData> _co2LevelDataList = [];

  Timer? _timer;

  @override
  void initState() {
    super.initState();
    initializeFirebase(); // Initialize Firebase
    _listenToDataChanges();
    _startTimer();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  Future<void> initializeFirebase() async {
    await Firebase.initializeApp();
  }

  void _listenToDataChanges() {
    _realtimeDatabase.child('input').onValue.listen((event) {
      var snapshotValue = event.snapshot.value;
      if (snapshotValue != null && snapshotValue is Map<dynamic, dynamic>) {
        var data = Map<String, dynamic>.from(snapshotValue);

        var co2Level = double.tryParse(data[' _CO2Level '].toString());
        var waterLevel = double.tryParse(data['_waterLevel '].toString());

        if (co2Level != null && waterLevel != null) {
          setState(() {
            _CO2Level = co2Level;
            _waterLevel = waterLevel;

            // Add the new data point to the list
            _waterLevelDataList.add(
              WaterLevelData(DateTime.now(), waterLevel),
            );
            _co2LevelDataList.add(
              CO2LevelData(DateTime.now(), co2Level),
            );
          });
        }

        print("data: $data");
        print("_CO2Level: $_CO2Level");
        print("_waterLevel: $_waterLevel");
      }
    });
  }

  void _startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 2), (timer) {
      setState(() {
        // Update the chart points every 2 seconds
        _waterLevelDataList.add(
          WaterLevelData(DateTime.now(), _waterLevel ?? 0.0),
        );
        _co2LevelDataList.add(
          CO2LevelData(DateTime.now(), _CO2Level ?? 0.0),
        );
      });
    });
  }

  charts.Series<CO2LevelData, DateTime> createChartSeriesCO2() {
    return charts.Series<CO2LevelData, DateTime>(
      id: 'Co2Level',
      colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xFF2A518E)),
      domainFn: (CO2LevelData data, _) => data.timestamp,
      measureFn: (CO2LevelData data, _) => data.co2Level,
      data: _co2LevelDataList,
    );
  }

  charts.Series<WaterLevelData, DateTime> createChartSeriesWater() {
    return charts.Series<WaterLevelData, DateTime>(
      id: 'WaterLevel',
      colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xFF2A518E)),
      domainFn: (WaterLevelData data, _) => data.timestamp,
      measureFn: (WaterLevelData data, _) => data.waterLevel,
      data: _waterLevelDataList,
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF2A518E),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text(
          'Dashboard',
          style: TextStyle(
            fontSize: 20.0,
            color: Colors.white,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: GestureDetector(
              onTap: () {
                Scaffold.of(context).openDrawer(); // Open the drawer
              },
              child: Container(
                padding: const EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: const Color(0xFFE0E8F3),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Icon(
                  Icons.dashboard,
                  color: Color(0xFF2A518E).withOpacity(0.8),
                  size: 20,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Colors.blue, Colors.purple],
          ),
        ),
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                const SizedBox(
                  height: 20.0,
                ),
                Row(
                  children: [
                    const SizedBox(width: 40),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      height: 150.0,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Stack(
                            children: <Widget>[
                              Container(
                                color: Colors.white.withOpacity(0.5),
                              ),
                              LiquidLinearProgressIndicator(
                                value: _waterLevel != null ? _waterLevel! / 100 : 0.0,
                                valueColor: AlwaysStoppedAnimation(
                                  Color(0xFF1E427D).withOpacity(0.7),
                                ),
                                backgroundColor: Colors.transparent,
                                direction: Axis.vertical,
                              ),
                              Positioned.fill(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      _waterLevel != null
                                          ? '${_waterLevel!.toStringAsFixed(1)}'
                                          : 'N/A',
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 24.0,
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                    Column(
                      children: [
                        Text(
                          _CO2Level != null ? _CO2Level!.toInt().toString() : 'N/A',
                          style: const TextStyle(fontSize: 50.0, color: Colors.white,fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,),
                        ),
                        Text(
                          _CO2Level != null ? '.${_CO2Level!.toStringAsFixed(2).split('.')[1]}' : '',
                          style: const TextStyle(fontSize: 34.0, color: Colors.white,fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,),
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20.9),
                          child: Text(
                            'PPM',
                            style: TextStyle(fontSize: 20.0, color: Colors.white,fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w500,),
                            textDirection: TextDirection.rtl,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 53)
                  ],
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    children: const [
                      Text(
                        'Water Level',
                        style: TextStyle(fontSize: 24.0, color: Colors.white,fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,),
                      ),
                      Expanded(
                        child: Center(
                          child: SizedBox(
                          ),
                        ),
                      ),
                      Text(
                        'CO2 Level',
                        style: TextStyle(fontSize: 24.0, color: Colors.white,fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(140, 10, 140, 0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    height: 5.0,
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Text(
                    'Water level curve',
                    style: TextStyle(fontSize: 20.0, color: Colors.white,fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: const EdgeInsets.all(8.0),
                    // Add padding to the container
                    height: 200.0,
                    child: charts.TimeSeriesChart(
                      [createChartSeriesWater()],
                      animate: true,
                      dateTimeFactory: const charts.LocalDateTimeFactory(),
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
                  child: Text(
                    'Water level curve',
                    style: TextStyle(fontSize: 20.0, color: Colors.white,fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: const EdgeInsets.all(8.0),
                    // Add padding to the container
                    height: 200.0,
                    child: charts.TimeSeriesChart(
                      [createChartSeriesCO2()],
                      animate: true,
                      dateTimeFactory: const charts.LocalDateTimeFactory(),
                    ),
                  ),
                ),

                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(
                    'Camera',
                    style: TextStyle(fontSize: 20.0, color: Colors.white,fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    height: 200.0,
                    child: const Center(
                      child: Text(
                        'Camera View',
                        style: TextStyle(fontSize: 20.0, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [Colors.blue, Colors.purple],
                ),
              ),
              child: Text(
                'DashBoard',
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                ),
              ),
            ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => Menu()),
                );
              },
              trailing: Icon(Icons.arrow_forward),
            ),
            ListTile(
              title: Text('Control'),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => Control()),
                );
              },
              trailing: Icon(Icons.arrow_forward),
              // Add trailing arrow icon to indicate this is a main item.
            ),
          ],
        ),
      ),
    );
  }
}
